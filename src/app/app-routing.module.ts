import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { AuthGuard } from './guard/auth.guard';
import { DefaultLayoutComponent } from './layout/default-layout/default-layout.component';


const routes: Routes = [
  { path : '', redirectTo : 'products', pathMatch : 'full'},
  { path : '', component : DefaultLayoutComponent, canActivate: [AuthGuard], children : [
    { path : 'products', loadChildren: () => import('./module/product/product.module').then(m => m.ProductModule) },
    { path : 'orders', loadChildren: () => import('./module/order/order.module').then(m => m.OrderModule) }
  ]},
  { path : 'login', component: LoginComponent },
  { path : 'register', component: RegisterComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
