import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CommonModalService } from 'src/app/module/shared/service/common-modal.service';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

  alert = {
    type: null,
    isOpen: false,
    message: null
  }

  constructor(private authService: AuthService, private modalService: CommonModalService) { }

  form = new FormGroup({
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  })

  ngOnInit(): void {
  }

  register() {
    this.alert.isOpen = false;
    this.form.markAsDirty();
    if (this.form.valid) {
      let ref = this.modalService.showLoading("Registering");
      this.authService.register(this.form.controls.email.value, this.form.controls.password.value).subscribe(d => {
        console.log(d);
        ref.hide();
        if(d.error){
          this.showErrorAlert(d.error);
        }
        else if(d.message){
          this.showInfoAlert(d.message);
        }
      }, err => {
        console.log(err);
        ref.hide();
        this.showErrorAlert(err.error.error)
      })
    }
  }

  private showErrorAlert(message: string) {
    this.showAlert("danger", true, message);
  }

  private showInfoAlert(message: string) {
    this.showAlert("info", true, message);
  }

  private showAlert(type: string, isOpen: boolean, message: string) {
    this.alert.type = type
    this.alert.message = message
    this.alert.isOpen = isOpen;
  }


}
