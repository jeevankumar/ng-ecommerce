import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { OrderService } from 'src/app/module/order/service/order.service';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-default-layout',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.sass'],
})
export class DefaultLayoutComponent implements OnInit {

  totalOrders: number;
  email: string

  constructor(private orderService: OrderService, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.email = this.authService.email;
    this.loadData();
  }

  loadData(){
    this.orderService.findAllInitiated().subscribe(d =>
      this.totalOrders = d.length
    );
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['login']);
  }

}
