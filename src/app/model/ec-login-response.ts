import { EmailValidator } from "@angular/forms";
import { EcResponse } from "./ec-response";

export class EcLoginResponse extends EcResponse {
  email : string
  token : string
}
