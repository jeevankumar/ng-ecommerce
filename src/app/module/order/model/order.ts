import { Product } from "../../product/model/product";

export class Order {
  _id : string;
  status : string;
  totalAmount : number;
  discount : number;
  products : Product[]
}
