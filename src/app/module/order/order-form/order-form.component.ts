import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Product } from '../../product/model/product';
import { ProductService } from '../../product/service/product.service';
import { LoadingModalComponent } from '../../shared/component/loading-modal/loading-modal.component';
import { CommonModalService } from '../../shared/service/common-modal.service';
import { Order } from '../model/order';
import { OrderService } from '../service/order.service';

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.sass']
})
export class OrderFormComponent implements OnInit {

  order: Order;

  constructor(private orderService: OrderService,
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private modalService: CommonModalService) { }

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.params.id) {
      this.loadOrder();
    }
  }

  loadOrder() {
    this.orderService.findById(this.activatedRoute.snapshot.params.id).subscribe(data => {
      this.order = data;
    })
  }

  removeFromOrder(productId: string) {
    let mref = this.modalService.showLoading("Removing Product");
    this.productService.removeFromOrder(productId).subscribe(d => {
      this.loadOrder();
      mref.hide();
    }, err => {
      console.log(err);
      mref.hide();
    })
  }

  incrementQuantity(p: Product) {
    p.quantity += 1;
    this.updateOrder(p._id, p.quantity);
  }

  decrementQuantity(p: Product) {
    if (p.quantity > 0) {
      p.quantity -= 1;
      this.updateOrder(p._id, p.quantity);
    } else {
      this.removeFromOrder(p._id);
    }
  }

  private updateOrder(productId: string, quanity) {
    let modalRef = this.modalService.showLoading("Updating Product Quantity");
    this.productService.updateOrder(productId, quanity).subscribe(d => {
      console.log(d)
      this.loadOrder();
      modalRef.hide()
    }, err => {
      console.log(err);
      modalRef.hide()
    })
  }

  public orderConfirmed(){
    let ref = this.modalService.showLoading("Order confirming");
    this.orderService.changeStatus(this.order._id, "ORDERED").subscribe(d => {
      this.loadOrder();
      ref.hide();
    }, err => {
      console.log(err);
      ref.hide();
    })
  }

}
