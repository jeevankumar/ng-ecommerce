import { Component, OnInit } from '@angular/core';
import { CommonModalService } from '../../shared/service/common-modal.service';
import { Order } from '../model/order';
import { OrderService } from '../service/order.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.sass']
})
export class OrderListComponent implements OnInit {

  orders: Order[]

  constructor(private orderService: OrderService, private modalService: CommonModalService) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(){
    this.orderService.findAll().subscribe(data => {
      this.orders = data;
    })
  }

  delete(id: string){
    let ref = this.modalService.showLoading("Adding Product to Order");
    this.orderService.delete(id).subscribe(d => {
      console.log(d);
      this.loadData();
      ref.hide();
    }, err => {
      console.log(err);
      ref.hide();
    })
  }

}
