import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Order } from '../model/order';
import { OrderService } from '../service/order.service';

@Component({
  selector: 'app-order-view',
  templateUrl: './order-view.component.html',
  styleUrls: ['./order-view.component.sass']
})
export class OrderViewComponent implements OnInit {

  order: Order;

  constructor(private orderService: OrderService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.params.id) {
      this.loadOrder();
    }
  }

  loadOrder() {
    this.orderService.findById(this.activatedRoute.snapshot.params.id).subscribe(data => {
      this.order = data;
    })
  }

}
