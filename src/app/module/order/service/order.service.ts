import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Order } from '../model/order';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private SERVICE_URL = "/orders";

  constructor(private httpClient: HttpClient) {}

  public findAll() : Observable<Order[]> {
    return this.httpClient.get<Order[]>(`${environment.BASE_URL + this.SERVICE_URL}`);
  }

  public findAllInitiated() : Observable<Order[]> {
    return this.httpClient.get<Order[]>(`${environment.BASE_URL + this.SERVICE_URL}/initiated`);
  }

  public findById(id?: number) : Observable<Order> {
    return this.httpClient.get<Order>(`${environment.BASE_URL + this.SERVICE_URL}/${id}`);
  }

  public save(order: Order) : Observable<any> {
    return this.httpClient.post(`${environment.BASE_URL + this.SERVICE_URL}`, order);
  }

  public changeStatus(orderId: string, status: string) : Observable<any> {
    return this.httpClient.get(`${environment.BASE_URL + this.SERVICE_URL}/${orderId}/${status}`);
  }

  public delete(id: string) : Observable<any> {
    return this.httpClient.delete(`${environment.BASE_URL + this.SERVICE_URL}/${id}`);
  }

}
