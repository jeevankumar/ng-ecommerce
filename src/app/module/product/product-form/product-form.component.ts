import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../model/product';
import { ProductService } from '../service/product.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.sass']
})
export class ProductFormComponent implements OnInit {

  product: Product;

  alert = {
    type: null,
    isOpen: false,
    message: null
  }


  formGroup: FormGroup;

  constructor(private fb: FormBuilder, private productService: ProductService, private activatedRoute: ActivatedRoute) {
    this.formGroup = fb.group({
      'title': ['', Validators.required],
      'price': [0, Validators.required]
    })
  }

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.params.id) {
      this.productService.findById(this.activatedRoute.snapshot.params.id).subscribe(data => {
        this.product = data;
        this.initializeForm();
        console.log(this.formGroup.value)
      });
    } else {
      this.product = new Product();
    }
  }

  reset() {
    this.formGroup.reset();
    this.initializeForm()
  }

  initializeForm() {
    this.formGroup.controls.title.setValue(this.product.title);
    this.formGroup.controls.price.setValue(this.product.price);
  }

  save() {
    // console.log(this.formGroup.value);
    this.alert.isOpen = false;
    this.formGroup.markAsDirty();
    if (this.formGroup.valid) {
      this.product.title = this.formGroup.controls.title.value;
      this.product.price = this.formGroup.controls.price.value;
      this.productService.save(this.product).subscribe(data => {
        console.log(data);
        this.showSuccessAlert(`Product "${this.product.title}" added successfully.`);
        this.formGroup.markAsPristine();
      });
    }
  }

  private showErrorAlert(message: string) {
    this.showAlert("danger", true, message);
  }

  private showSuccessAlert(message: string) {
    this.showAlert("success", true, message);
  }

  private showAlert(type: string, isOpen: boolean, message: string) {
    this.alert.type = type
    this.alert.message = message
    this.alert.isOpen = isOpen;
  }


}
