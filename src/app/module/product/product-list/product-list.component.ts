import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { OrderService } from '../../order/service/order.service';
import { CommonModalService } from '../../shared/service/common-modal.service';
import { Product } from '../model/product';
import { ProductService } from '../service/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.sass'],
  host : { class : 'container'}
})
export class ProductListComponent implements OnInit {

  products : Product[]

  constructor(private productService: ProductService, private orderService: OrderService ,private modalService: CommonModalService) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(){
    this.productService.findAll().subscribe((data) => {
      this.products = data;
    })
  }

  addToOrder(id: string){
    let ref = this.modalService.showLoading("Adding Product to Order");
    this.productService.addToOrder(id).subscribe(d => {
      console.log(d);
      ref.hide();
    }, err => {
      console.log(err);
      ref.hide();
    })
  }

  delete(id: string){
    let ref = this.modalService.showLoading("Adding Product to Order");
    this.productService.delete(id).subscribe(d => {
      console.log(d);
      this.loadData();
      ref.hide();
    }, err => {
      console.log(err);
      ref.hide();
    })

  }

}
