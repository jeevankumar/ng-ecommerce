import { Injectable } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LoadingModalComponent } from '../component/loading-modal/loading-modal.component';

@Injectable({
  providedIn: 'root'
})
export class CommonModalService {

  constructor(private bsModalService: BsModalService) {

  }

  showLoading(message: string = null) : BsModalRef<any> {
    return this.bsModalService.show(LoadingModalComponent, {
      animated: true,
      backdrop: "static",
      keyboard: false,
      initialState : {
        message : message
      }
    });
  }
}
