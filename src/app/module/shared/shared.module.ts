import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputHelperComponent } from './component/input-helper/input-helper.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { LoadingModalComponent } from './component/loading-modal/loading-modal.component';



@NgModule({
  declarations: [InputHelperComponent, LoadingModalComponent],
  imports: [
    CommonModule,
    ModalModule.forRoot()
  ],
  bootstrap : [ LoadingModalComponent ],
  exports : [ InputHelperComponent ]
})
export class SharedModule { }
